# GENERAL REPORT

### Copy app folder (Tutorial React.js and Spring Data REST) to the repository.

In this step can't forget to *delete* .git folder if exists.

### ADD, COMMIT and PUSH changes.

'git add' command to add changed file to a Staging Area (intermediate area between last solid version and the changed version).
If use .(dot sign) as parameter will add all files(e.g. git add .). Or can specify the files as parameter (e.g. git add FILE)

'git commit -m 'something to say about the changes...' ' command to certified the changes.

*Must have* a message to be done.

'git push' command to send this new version to the remote repository.

In this last command, if is the first one, need to identify the local and remote repository, using ´git push origin master' command. 
This will associate local (master) and remote (origin) repository to send information between them. 
Additionally can use '-u' parameter to "save" this association (e.g. git push -u origin master) and then the next push commands do not need 'origin master' part.

