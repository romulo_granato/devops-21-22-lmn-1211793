/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private int jobYears;
	private String email;


	/**
	 * Constructors.
	 */

	private Employee() {}

	public Employee(String firstName, String lastName, String description, int jobYears, String email) {

		checkNullStringParams(firstName, lastName, description, email);
		checkEmptyBlankStringParams(firstName, lastName, description, email);
		checkLengthRulesStringParams(firstName, lastName, description);
		checkNegativeNumber(jobYears);
		checkAtSign(email);

		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.jobYears = jobYears;
		this.email = email;
	}

	/**
	 * Private methods to validate entry data to create a new object.
	 */

	private void checkNullStringParams(String fn, String ln, String d, String e) {
		if (fn == null || ln == null || d == null || e == null){
			throw new IllegalArgumentException("First Name, Last Name or Description cannot be null.");
		}
	}

	private void checkEmptyBlankStringParams(String fn, String ln, String d, String e) {
		if (fn.trim().isEmpty() || ln.trim().isEmpty() || d.trim().isEmpty() || e.trim().isEmpty()){
			throw new IllegalArgumentException("First Name, Last Name or Description cannot be empty/blank.");
		}
	}

	private void checkLengthRulesStringParams(String fn, String ln, String d) {
		if (fn.length() < 2 || ln.length() < 2 || d.length() < 3){
			throw new IllegalArgumentException("First Name and Last Name must have at least 2 characters. Description must have at least 3 characters.");
		}
	}

	private void checkNegativeNumber(int jy) {
		if(jy < 0){
			throw new IllegalArgumentException("Job Years cannot have negative number.");
		}
	}

	private void checkAtSign(String email) {
		if (!email.contains("@")){
			throw new IllegalArgumentException("Email must have at sign.");
		}
	}

	/**
	 * Getters and Setters methods.
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getJobYears() {return jobYears;}

	public void setJobYears(int jobYears) {this.jobYears = jobYears;}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return jobYears == employee.jobYears && Objects.equals(id, employee.id) && Objects.equals(firstName, employee.firstName) && Objects.equals(lastName, employee.lastName) && Objects.equals(description, employee.description) && Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, description, jobYears, email);
	}

	@Override
	public String toString() {
		return "Employee{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", description='" + description + '\'' +
				", jobYears=" + jobYears +
				", email='" + email + '\'' +
				'}';
	}
}
// end::code[]
