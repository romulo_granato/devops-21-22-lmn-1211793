package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void emptyFirstNameTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankFirstNameTest1(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= " ";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankFirstNameTest2(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "   ";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void nullFirstNameTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= null;
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void emptyEmailTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankEmailTest1(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= " ";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankEmailTest2(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "    ";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void nullEmailTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= null;
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesFirstNameFailTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "G";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesFirstNameBorderSuccessTest(){
        //Arrange
        String fName= "Ga";
        String lName= "Gandalf";
        String desc= "White Sorcerer";
        String email= "gan@lor.com";
        int jYears = 0;
        //Act
        Employee test = new Employee(fName, lName, desc, jYears, email);
        //Assert
        assertEquals(fName,test.getFirstName());
    }

    @Test
    void emptyLastNameTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankLastNameTest1(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= " ";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankLastNameTest2(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "   ";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void nullLastNameTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= null;
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesLastNameFailTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "G";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesLastNameBorderSuccessTest(){
        //Arrange
        String fName= "Gandalf";
        String lName= "Ga";
        String desc= "White Sorcerer";
        String email= "gan@lor.com";
        int jYears = 0;
        //Act
        Employee test = new Employee(fName, lName, desc, jYears, email);
        //Assert
        assertEquals(lName,test.getLastName());
    }

    @Test
    void emptyDescriptionTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankDescriptionTest1(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= " ";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void blankDescriptionTest2(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "   ";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void nullDescriptionTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= null;
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesDescriptionFailTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "W";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesDescriptionBorderFailTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "Wh";
            String email= "gan@lor.com";
            int jYears = 0;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void lengthRulesDescriptionBorderSuccessTest(){
        //Arrange
        String fName= "Gandalf";
        String lName= "Gandalf";
        String desc= "Whi";
        String email= "gan@lor.com";
        int jYears = 0;
        //Act
        Employee test = new Employee(fName, lName, desc, jYears, email);
        //Assert
        assertEquals(desc,test.getDescription());
    }

    @Test
    void notPositiveNumberTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gan@lor.com";
            int jYears = -1;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

    @Test
    void positiveNumberSuccessBorderTest(){
        //Arrange
        String fName= "Gandalf";
        String lName= "Gandalf";
        String desc= "White Sorcerer";
        String email= "gan@lor.com";
        int jYears = 0;
        //Act
        Employee test = new Employee(fName, lName, desc, jYears, email);
        //Assert
        assertEquals(jYears,test.getJobYears());
    }

    @Test
    void createEmployeeSuccessTest(){
        //Arrange
        String fName= "Gandalf";
        String lName= "Gandalf";
        String desc= "White Sorcerer";
        String email= "gan@lor.com";
        int jYears = 3;
        //Act
        Employee test = new Employee(fName, lName, desc, jYears, email);
        //Assert
        assertEquals(fName,test.getFirstName());
    }

    @Test
    void emailWithoutAtSingTest(){
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            //Arrange
            String fName= "Gandalf";
            String lName= "Gandalf";
            String desc= "White Sorcerer";
            String email= "gandalf.com";
            int jYears = 1;
            //Act
            new Employee(fName, lName, desc, jYears, email);
        });
    }

}