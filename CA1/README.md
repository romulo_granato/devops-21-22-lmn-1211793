# CA1 Report

## Version Control System (VCS)

In software engineering, version control is a class of systems responsible for managing changes to computer programs, documents, large web sites, or other collections of information. 
Changes are usually identified by a number or letter code, termed the "revision number", "revision level", or simply "revision". 
For example, an initial set of files is "revision 1". When the first change is made, the resulting set is "revision 2", and so on. 
Each revision is associated with a timestamp and the person making the change. Revisions can be compared, restored, and, with some types of files, merged.

## GIT VCS tool

It was released in 2005 for development of the Linux kernel. Its goals include speed, data integrity, and support for distributed, non-linear workflows (thousands of parallel branches running on different systems).

As with most other distributed version control systems, every Git directory on every computer is a full-fledged repository with complete history and full version-tracking abilities

Git has two data structures: a mutable index (also called stage or cache) that caches information about the working directory and the next revision to be committed; and an immutable, append-only object database.

The index serves as a connection point between the object database and the working tree.

The object database contains five types of objects: blob, tree, commit, tag and packfile.

A blob (binary large object) is the content of a file. In git each blob is a version of a file, it holds the file's data.

A tree object is the equivalent of a directory. It contains a list of file names, each with some type bits and a reference to a blob or tree object. These objects are a snapshot of the source tree.

A commit object links tree objects together into history. It contains the name of a tree object, a timestamp, a log message, and the names of zero or more parent commit objects.

A tag object is a container that contains a reference to another object and can hold added meta-data related to another object. Most commonly, it is used to store a digital signature of a commit object.

A packfile object is a zlib version compressed of various other objects for compactness and ease of transport over network protocols.

Each object is identified by a SHA-1 hash of its contents. Git computes the hash and uses this value for the object's name.

Git stores each revision of a file as a unique blob. The relationships between the blobs can be found through examining the tree and commit objects.

Additionally, git stores labels called refs (short for references) to indicate the locations of various commits. They are stored in the reference database and are respectively:

Heads (branches): Named references that are advanced automatically to the new commit when a commit is made on top of them.

HEAD: A reserved head that will be compared against the working tree to create a commit.

Tags: Like branch references but fixed to a particular commit. Used to label important points in history.

### Main commands

#### GIT CONFIG

Tell Git who you are. For example, I can use 'git config --global user.name "any name"' to define your name to Git.

#### GIT INIT

Create a new local repository.

#### GIT CLONE

Create a working copy of a repository.

#### GIT ADD

Add one or more files to staging area.

To add all changed files to staging area, use '.(dot)' parameter to git add command, like 'git add .'.

#### GIT COMMIT

Commit changes to head (but not yet to the remote repository).

#### GIT PUSH

Send changes to your remote repository. Need to identify the branches in your command, for example 'git push origin master' to send the changes of your local master branch to remote master(called origin) branch.

To push the tag to remote repository, need to add '--tags' to git push command, like 'git push --tags'.

#### GIT STATUS

List the files you've changed and those you still need to add or commit.

#### GIT REMOTE

If you haven't connected your local repository to a remote server, add the server to be able to push to it. If you has used git clone from a remote repository, the remote repository are already set.

#### GIT BRANCH

To create a new branch to your local repository: 'git branch "new branch"'

To list all branches: 'git branch'.

To delete a specific branch: 'git branch -d "branch name".

#### GIT CHECKOUT

To switch to another branch: 'git checkout "another branch".

To create and switch to a new branch: 'git checkout -b "new branch".

#### GIT MERGE

To merge a different branch into your active branch (HEAD).

#### GIT PULL

Fetch and merge changes on the remote server to your working directory (local repository).

#### GIT TAG

To create a tag to short identification of this commit.

To create a tag associanting to a exist commit(e.g. git tag v.1.1.0 CHECKSUM -m 'message'), but not the commit that you are.

To delete a created tag:'git tag -d "TAG NAME"'.

To replace a existing tag to HEAD commit: 'git tag -f "TAGNAME".

#### GIT LOG

To show all commits of my project.

To show only a limit number of the commits list: 'git log -"NUMBER OF COMMITS"'.

### CA STEPS

#### PART 1

Firstly I created a new remote repository called devops-21-22-lmn-1211793 on bitbucket. After that I did a clone to create a working directory with git clone command. Now I had a local repository connected to my remote one and can work on that.

To step 1 I create a new folder called CA1 on my local repository and copy the code of Tutorial React.JS App. To do this I used command line with mkdir and cp command, i.e. '$ mkdir CA1', 'cp -r "Tutorial React.JS APP" "CA1/".

To step 2 I add and commit the changes, using 'git add .' and 'git commit -m "initial commit resolve #1"' and push the changes to my remote repository using 'git push' command.

I do not need to set the remote server to local repository with git remote command because when I used git clone this configuration already be set.

To step 3 I create a new tag to mark the version of the app using 'git tag v1.1.0' command. After that I must push this tag to remote server using 'git push --tags'.

To step 4 I make changes to my app code and then add, commit the changes, create a new tag v1.2.0 and push them. Commands: 'git add .', 'git commit -m "any message", 'git tag v1.2.0'.

To step 5 I create a new tag to mark the end of this class assigment with 'git tag ca1-part1' and push them too (git push --tags).

#### PART 2

To PART2 class assigment, I need to use branches to develop new features of my APP without broke down my stable version that will be able on master branch.

To step 2 I created a new branch using 'git branch email-field' and then 'git checkout email-field' to switch to this new created branch. Now I can work in this new branch without make trouble to my master branch.

Still in step 2 I make changes to my app to create a new feature and test them. After all this changes are okay I add and commit them. Commands: 'git add .' and 'git commit -m "any message"'.

Then a created a new tag (git tag v1.3.0) to mark the end of this new feature.

To step 3 I need to create a new branch called fix-invalid-email and work on that. I use 'git checkout -b fix-invalid-email' to create and check out to this new branch.

I make the necessary changes and then 'git add .', 'git commit -m "any message"' and 'git tag v1.3.1'.

After all features are correctly I merge to my master branch and push them to my remote repository. To do that I used:

'git checkout master' to switch to my master branch.

'git merge fix-invalid-email' to merge with fix-invalid-email. As this branch (fix-invalid-email) was created with the stable (and last) commit of the email-field branch, it has all changes from that branch. So I do not need to merge this branch too.

In this step all changes with master branch are analyzed by GIT and if the system do not get resolve all differences (git diff), it will ask to user to resolve them. In my case I did not need to resolve anything.

Now I can push all new features (and changes) to my remote server. To do that I create a new tag to mark the ende of my class assigment using 'git tag ca1-part2'. Then I used 'git push --tags'.

As I want to register all branches created locally to my remote server, I push them too. To do that I use 'git push origin email-field' and 'git push origin fix-invalid-email'. Now all my branches are available on Bitbucket.

### General problems

A new .bak file was created to a README file. To solve this I add .bak to .gitignore file to commit ignore this files.

In test, if try to create a new employee object with any not acceptable parameter (e.g. empty first name), when run Spring Boot, the app will not be working.

## Others VCS tools

### Search VCS tools

In my search i found a lot of others tools to version control system. In the past the most popular was CVS and SVN tools. Today they still have relative importance, but the GIT tool is the most popular now.
It is faster and lighter then others and have a lot of support documentation. Because this the IT development teams use mostly GIT tool.

Optionally, Mercurial tool has many pros as GIT, like high performance, robustness and scalability. In addition to having similarities with the GIT tool itself. 
Because of that I chose Mercurial as an alternative tool to implement in my app.

### Mercurial VCS tool

It is supported on Microsoft Windows and Unix-like systems, such as FreeBSD, macOS, and Linux. It has also taken steps to ease the transition for users of other version control systems.

Mercurial is primarily a command-line driven program (like Git), but graphical user interface extensions are available and several IDEs offer support for version control with Mercurial.
All of Mercurial's operations are invoked as arguments to its driver program hg (a reference to Hg – the chemical symbol of the element mercury).

It is UI friendly(includes an integrated web-interface), less complexity and can use prefixes of subcommands, e.g. 'hg up' for 'hg update'. Your language code is mostly Python and all add-ons must be written in it.

#### Principal commands

The main commands are the same as in GIT, just swapping git for hg. For example:

git clone -> hg clone
git init -> hg init
git add -> hg add (in this case do not need to use '.(dot)' params to add all files to commit them later, like GIT)
git commit -> hg commit (need to write a message to complete, like GIT. Use default text editor if don't specify in command line or use -m params to write on it)
git push -> hg push
git log -> hg log
git status -> hg status
git diff -> hg diff
git tag -> hg tag
git branch -> hg branch
git merge -> hg merge

#### Online repository

To host online my app with Mercurial VCS, I chose sourceforge.net. (https://sourceforge.net/projects/devops-21-22-lmn-1211793/)

#### Get Started

To windows OS need to install TortoiseHg.

Use hg clone to copy remote repository to local.

After that copy my app (without .git folder) to Mercurial repository.

'hg add' command to add all files to staging area to commit later.

'hg commit -m 'any message' to commit all changes.

'hg status' to see if has any changed file without commit or add command.

'hg log' to see the commit history of my project.

'hg push' to send all committed changes to remote repository.

'hg branch (BRANCH NAME)' to create a new branch.

