# CA3 Report

In this CA we must to understand how Virtual Machine works and build some of them. For this I must use VirtualBox tool and Vagrant.

## VirtualBox Tool

It supports the creation and management of guest virtual machines running Windows, Linux, BSD, OS/2, Solaris, Haiku, and OSx86, as well as limited virtualization of macOS guests on Apple hardware.

VirtualBox is free and open-source software, though the Extension Pack is proprietary software.

Users of VirtualBox can load multiple guest OSes under a single host operating-system (host OS). Each guest can be started, paused and stopped independently within its own virtual machine (VM).
The user can independently configure each VM and run it under a choice of software-based virtualization or hardware assisted virtualization if the underlying host hardware supports this.
The host OS and guest OSs and applications can communicate with each other through a number of mechanisms including a common clipboard and a virtualized network facility. 
Guest VMs can also directly communicate with each other if configured to do so.

## Part 1

### Virtualization steps

After installed VirtualBox in my PC, I can start them and begin to build a new virtual machine (VM).

Following the boxes that showing me what to do to create a new VM. I can choose a VM name, OS, memory size, hard disk type and more. 

When I was building my VM, I can not choose a 64-bits OS and after finished, I can not start the VM. 
This happend because my PC was not configured to permit a VM build. 
To correct that, I must go to my BIOS setup and enable virtualization. Then I can create and run a new VM correctly (with 64-bits).

To run a created VM, I only need to choice the VM that I wanted in the showing list and then press START. Now the VM are running and a new window with the interface are open.
a
When the VM is initiate at the first time, I must lnd install and configure UBUNTU. Basically follow the commands on the display.

#### VM running 
To setup the VM to my work, I must install some funcionalities, like openssh-server, vsftpd, git and java, using command sudo apt install.

Open SSH Server enable the host computer to access the VM by terminal.

FTP protocol enable to transfer files between the host computer and the VM.

GIT and JAVA are used to the projects that I will clone to the VM.

### Run Projects 

With VM configured I can clone my personal repository to run some projects inside that. To do that I go to VM terminal and use git clone command inside a choosen directory. Now I had my remote repository inside th VM.

#### Spring Boot React Basic Project

To run this project I need to install maven package. SUDO APT INSTALL MAVEN. 

Unlike the version of this project on the host computer, I can not use MVNW (maven wrapper) to run the APP. Because of this I needed to install maven. 

I can use MVN command to run spring-boot. MVN SPRING-BOOT:RUN.

Because the VM does not have a graphic interface, to show the project running I need to go to browser of the host computer. There I can digit the IP of the VM (192.168.56.13) and the port (8080) of the server to show de project working. http://192.168.56.13:8080.

Because the server are running in the VM and de host computer is on the same network of that (I configure that when created the VM), I can do these steps.

#### Gradle Basic Project

TO run this project I need to install GRADLE package. SUDO APT INSTALL GRADLE.

Unlike the version of this project on the host computer, I can not use GRADLEW (gradle wrapper) to run the APP. Because of this I needed to install gradle.

Here I have a lot of problems. Despite having updated the APT, when I ask to install the gradle, it installed a old version (4.4.1) of that. With this version I can not run my GRADLE BASIC PROJECT because some arguments/functions are not recognized. 

To fix this, I need to install gradle manually with 7.4.2 version. I follow some steps founded here: https://linuxize.com/post/how-to-install-gradle-on-ubuntu-18-04/.

After that I can run my project using GRADLE command.

To run the server side. GRADLE runServer.

Because the VM does not have a graphic interface,  I need to run a client side APP in the host computer. To do that I need to edit the runClient task of the build.gradle file on the host computer to connect with the server using the IP of the VM, 192.168.56.13.

Now I can run a server side in the VM and a client side in the host computer (or other with graphic interface, as long as it's on the same network).

## Part 2

### Vagrant Tool

Vagrant is an open-source software product for building and maintaining portable virtual software development environments. It tries to simplify the software configuration management of virtualization in order to increase development productivity.
Vagrant is written in the Ruby language, but its ecosystem supports development in a few other languages.

Vagrant provides easy to configure, reproducible, and portable work environments built on top of industry-standard technology and controlled by a single consistent workflow to help maximize the productivity and flexibility of you and your team.

#### Main concepts

It need to be installed, as well as a virtual machine creation tool, like VirtualBox, VMWare and etc.

To install VAGRANT go to https://www.vagrantup.com/downloads and download the Amd64 version if has 64-bits OS and choice it OS (Windows, Linux, MacOS, etc.). Then install following the steps presented. To confirm that, go to terminal and use vagrant -v command.

Now you can start a new project using vagrant.

Vagrant uses a VagrantFile as a script to automate the process of create a Virtual Machine and configure that. To create a new vagrantFile, use VAGRANT INIT {BOX NAME}.

BOX is a VM pre-configured present on the WEB (https://app.vagrantup.com/boxes/search). To use vagrant you must already have a box created and available on the web. The tool will use this box to create the VM and then configure that as you wish.

With this new vagrantFile created, it can add the configuration that you need, like memory size, install tools, define IP and etc.

After that, to run the vagrantFile and create the VM (or VMs), it just need to be in the same directory of the vagrantFile on terminal and use VAGRANT UP command. 

Now the tool will process all information inside the file and create and configure the VM. If anything okay, the VM will be running on VM tool chosen (VirtualBox is the default provider). And will be able in that tool. 

To use other provider just set VAGRANT_DEFAULT_PROVIDER to the provider you wish to be the default. For example, if you use Vagrant with VMware Fusion, you can set the environmental variable to vmware_desktop and it will be your default.

#### Steps

Step 1 - It use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as reference to the vagrantFile.

Step 2 - It study the vagrantFile and identify what is happening.

Inside that it can be found two VMs (db and web) and your configuration. For example, the file has some commands for both machines and some commands for each one. It can see the configuration of the IPs and ports too. And at the end it can found some comands inside "web" VM to "install" the APP that will be tested.

All these commands and sections are between DO/END statements. Each one to a specific cenario, like provisions to all machines, provisions only to "db" machine and only to "web" machine.

Step 3 - Copy this vagrantFile to my repository (to will be used as enviroment builder of the app).

Step 4 - Update some steps of the file to adapt that to my project. For example, change JAVA version (8 to 11, because this APP need JAVA 11 to run) and change the last steps to consider my project.

Because JAVA 11 are not supported by ubuntu-xenial BOX, it must be switch to other box available. I chose "hashicorp/bionic64".

Step 5 - Because it uses two VM (to db and webserver), I must change my project to consider that. It generated modification at build.gradle, application.properties and ServeletInitialize file.

It use https://bitbucket.org/atb/tut-basic-gradle/src/master/ as reference to these changes.

Application.properties - has the main changes to configure the app to use H2 database in other VM.

build.gradle - adapt dependency to uses TOMCAT and to build a .WAR file.

ServeletInitialize - a new Java class to configure the SERVELET used by APP.

If anything wrong, when do VAGRANT UP on the terminal, it will create the VMs and run the APP on webServer VM and database on another VM. After that I can test the application using a host browser with the following paths:

Open the spring web application using one of the following options:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
You can also open the H2 console using one of the following urls:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
For the connection string use: jdbc:h2:tcp://192.168.56.11:9092/./jpadb

### Alternative Tools

The virtualization concept can be simply considered as one computer within another. It will allow us to use multiple operating systems on a single computer or machine. 

Platform virtualization software, specifically emulators and hypervisors, are software packages that emulate the whole physical computer machine, often providing multiple virtual machines on one physical platform.

We use VirtualBox as first choice to create a virtual machine. It is one of the most popular virtualization software. The others are VMware, QEMU, Parallels Desktop, Citrix Hypervisor and more. Each one has pros and cons, as which host and guest OS support and if has a open source free version.

Vagrant has specific support for only some providers as VirtualBox, VMware, Hyper-V and Docker. Because of that, I will analize and implement an alternative with VMware tool.

#### VMware Desktop

First install VMware Workstation Player (an free trial application) https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html

Then the installation of the Vagrant VMware provider requires two steps. First the Vagrant VMware Utility must be installed (https://www.vagrantup.com/vmware/downloads).

The Vagrant VMware Utility provides the Vagrant VMware provider plugin access to various VMware functionalities. The Vagrant VMware Utility is required by the Vagrant VMware Desktop provider plugin.

Next, install the Vagrant VMware provider plugin using the standard plugin installation procedure: $ vagrant plugin install vagrant-vmware-desktop.

Now I can change the vagrantFile to use VMware as provider.

Create a new vagrantFile. VAGRANT INIT HASHICORP/BIONIC64

Copy the script of the last vagrantFile to this. Because I use a box that already run with VMware, I do not need to change de box.

To chose a correct provider to this file, I can do one of the following steps:

Option 1 - when run vagrant up on terminal I can specify the provider that I want. VAGRANT UP --PROVIDER VMWARE_DESKTOP

Option 2 - in the shell I can set the VAGRANT_DEFAULT_PROVIDER environmental variable.

Option 3 - I can configure in the vagrantFile a provider. CONFIG.VM.PROVIDER "VMWARE_DESKTOP.

I chose the option 1 and do not need to change my vagrantFile to use vmware provider.

I must need to change the network configuration because was trying to up with last configuration and show me a error message with IP conflicts.

Now I can up the new machines with VMWARE provider. 

NOTE: When I try to create the VMs show me an UNKNOWN ERROR. In my search I read about this error and can be something with the VMware version or something like that. 
