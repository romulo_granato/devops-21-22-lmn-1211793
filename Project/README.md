# PROJECT - TRACK 9 Report

In this TRACK I must to deploy our APP on the DEI CLOUD (where each part of the app - web and db - will be host on differents virtual machines(VM)) with MAVEN, VAGRANT, ANSIBLE and JENKINS.

Repository: https://bitbucket.org/switch-2021/projectg2_devops
Branch: 1211793Track9

## Concept Steps

**Step 1** - Create and configure two VM on DEI CLOUD:

The first VM will host TOMCAT and will be able to run the application.

The second VM will host PostgreSQL database anb will be able to be accessed by APP to store and consult.

Inside Web Interface of DEI Virtual Servers Private Cloud I can create new VMs using one of the templates available.

After chose them I can go to each one and start the VMs. Now the VMs are available.

Copy the address, user and password of these VMs to configure the application and the Ansible.

Both VMs must have PYTHON and SSH installed. PYTHON are used by ANSIBLE to manage this VMs. SSH are used to be able to access this VMs from other computer.

Access the VMs using SSH from localhost and update APT-GET, install PING and install PYTHON.

*$ sudo apt-get update -y*
*$ sudo apt-get install iputils-ping -y*
*$ sudo apt-get install python3 --yes*

To install POSTGRESQL I can follow the below steps, but I had some problems with the conection to the APP. I was unable to correctly configure access.

$ sudo apt install lsb-core
$ apt-get install wget
Follow: https://www.postgresqltutorial.com/postgresql-getting-started/install-postgresql-linux/
$ sudo /etc/init.d/postgresql restart

Alternatively, I can create a VM from DEI CLOUD that aldeady has PostgreSQL installed and running and use that to configure the APP.

Now the VMs are created and configured.

**Step 2** - Create, configure and run Vagrant:

With Vagrant I must create one VM locally that will be the control of the others VMs on the cloud.

Using Vagrantfile I can automate the creation and configuration of the control VM.

This VM must have some programs to run Ansible and Jenkins as PYTHON and JAVA. Also need to have ANSIBLE and JENKINS installed itself.

All these programs installation must be declared on Vagrantfile. The JENKINS will be installed with WAR file. Because of this, I must run jenkins inside VagrantFile to Jenkins Server be able to use.

With Vagrantfile ready I can run on terminal inside the folder that have this. At the end, the VM will be created and the Jenkins will be running and can be accessed on the browser using https://localhost:8081 (the port was defined on Vagrantfile)

*$ vagrant up*

The Vagrantfile is available in the root folder of the project.

**Step 3** - Initial configuration of Jenkins:

When Jenkins is running go to browser and enter with https://localhost:8081 (or other port defined on vagrantfile).

The Jenkins Server will show the initial page and must enter with default password created when Jenkins was initiated on terminal. The terminal will show this password and I must copy that to use. 

After enter the default password, the Jenkins Server will ask to install default plugins and create a new admin user to access it at others times.

Then the Jenkins will be available to be used.

**Step 4** - Configure Ansible and your Playbooks:

> *ansible.cfg* - Create this file to override default settings in Ansible. For example, I can set where the host file will be available using *"inventory = ${path}"*.

> *hosts* - Create a file named hosts and use that to configure the hosts of the ansible. This file will set some configurations to the VMs that will be managed by Ansible, as name, location/ssh, username, password and others.
Also can group hosts by similar patterns to simplify.

To this project I declared two hosts, called web and db, with name, ssh address, username and password.

> *playbook* - A YAML file that contains what the Ansible must do on the hosts. These files will execute some actions on the hosts. It can chose a host or group of hosts and declared some commands, like install, configure or run something.

I create a playbook to each VM on the cloud. To web VM the playbook must install TOMCAT and JAVA and start TOMCAT. To db VM the playbook must install and start POSTGRESQL.

Also create a playbook to deployment step that will be used by Jenkins.

The playbooks are available in the root folder of the project.

**Step 5** - Configure Application:

I need to make some changes on the application to use POSTGRESQL as database remotely. 

I change de POM dependency of the database to POSTGRESQL and change de application.properties file (/src/main/resources/) to set up the connection with the database.

I need to make some changes on the application to use TOMCAT external, because SPRING BOOT has embedded TOMCAT as default. 

I change de POM dependency to use external TOMCAT, change the packaging task to create a WAR file and change the SwitchApp class to uses that external TOMCAT.

**Step 6** - Create, congifure and run Jenkinsfile:

To automate the deployment of the APP, I used Jenkinsfile inside the project root directory that will be used by Jenkins Server to run the pipeline.

The Jenkinsfile has all necessary steps, as:

> *Checkout* - To clone the remote repository where is the project and all necessary files to execute this deployment.

> *Build* - To build the APP and create the WAR file and run all tests in the APP. *$ ./mvnw clean package*

> *Coverage Report* - To publish the coverage report of the tests on the Jenkins Server.

> *Create Javadoc* - To create a JAVADOC file of the APP.

> *Publish Javadoc* - To publish the JAVADOC file created earlier on the Jenkins Server

> *Archiving* - To archive WAR file of the APP on the Jenkins Server.

> *Deploy TOMCAT* - To run the playbook with Ansible that will install and execute TOMCAT on the cloud VM created.

> *Deploy APP* - To run the playbook with Ansible that will copy the WAR file created earlier to TOMCAT on the cloud VM.

The Jenkinsfile are available in the root folder of the project.

**Step 7** - Configure Jenkins Server and Create Pipeline:

To run the Jenkinsfile I need to create a Pipeline inside Jenkins Server and configure that to use the Jenkinsfile inside the remote repository.

Go to Jenkins Server GUI (https://localhost:8081). 

I need to configure some credentials (to enter on VM created by vagrant and to access bitbucket to clone the project).

Go to manage jenkins -> manage credentials -> (global) -> Add Credentials

Then need to add new plugins to run some commands on Jenkinsfile.

Go to manage jenkins -> manage plugins -> availables

Search and select: Ansible, Code Coverage API, HTML publisher. Install and restart.

Now Jenkins Server are ready to create and run the Pipeline.

Go to New Job

Type the name -> chose pipeline -> OK -> Pipeline -> Definition: Pipeline script from SCM -> SCM: Git -> enter Repository URL: https://romulo_granato@bitbucket.org/switch-2021/projectg2_devops -> branch: 1211793Track9 -> save

Then go to the JOB created and BUILD NOW.

If all steps are OKAY the build will be finished successfully.

But I had problems when the APP is initializing that I couldn't solve.

## Alternatives

**Track 1** - H2 are easier to configure with the project. More automate proccess because with two files (vagrantfile and jenkinsfile) can do almost everything to deploy the enviroment and the APP. Basically the scripts can be used for anyone, just by few steps.

Because uses local computer, the service of this deploy only be available while this computer are running.

**Track 2** - The difference is that the APP uses Gradle as build tool. Gradle has a format more simple, but Maven is better known. 

**Track 3** - The usage of Docker to create images and containers to WEB and DB are the simpliest way to configure this services and make it fast and light. This also simplify the usage of this enviroment by others.

But the manually process to execute the APP do not automate all needs.

**Track 4** - equal to track 6, but uses Gradle.

**Track 5** - The step that needs to deploy the APP on the cloud are manually, but can available the APP to others.

**Track 6** - equal to track 6, but uses Gradle.

**Track 7** - H2 are easier to configure with the APP  that aldeary had this database. The solution using CLOUD VMs are better to share the service and stand the APP available, but I think that the steps are more complicated and manually, like create and configure manually that machines.

**Track 8** - equal to track 6, but uses Gradle. 

Search for information on the web about how implement some features and fix some problems are easier with MAVEN. Gradle does not had many examples yet.

**Track 10** - uses GRADLE, but the experience are very similar.
