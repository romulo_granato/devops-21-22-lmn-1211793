package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.aggregates.Task.Task;


import java.util.List;
import java.util.Optional;

public interface ITaskRepo {

    /**
     * Finds all objects from this repository.
     * @return list
     */
    List<Task> findAll();

    /**
     * The repository should be able to find a object using given Identity.
     * @param code code
     * @return task
     */
    Task findById(String code);

    /**
     * @param newTask newTask
     * @return task
     */
    Optional<Task> save (Task newTask);


    /**
     * @param id id
     * @return boolean
     */
    boolean existById(String id);

}
