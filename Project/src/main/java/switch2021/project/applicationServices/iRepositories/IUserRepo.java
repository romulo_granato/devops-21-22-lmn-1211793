package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.aggregates.User.User;
import switch2021.project.entities.valueObjects.vos.UserID;
import switch2021.project.entities.valueObjects.vos.UserProfileID;
import java.util.List;
import java.util.Optional;

public interface IUserRepo{

    /**
     * @param id id
     * @return user
     */

    Optional<User> findByUserId(UserID id);

    /**
     * @return list
     */

    List<User> findAll();

    /**
     * @param name name
     * @return list
     */

    List<User> findAllByNameContains(String name);

    /**
     * @param function function
     * @return list
     */

    List<User> findAllByFunctionContains(String function);

    /**
     * @param profile profile
     * @return list
     */

    List<User> findAllByUserProfileId(UserProfileID profile);

    /**
     * @param id id
     * @return boolean
     */

    boolean existsById(UserID id);

    /**
     * @param user user
     * @return user
     */

    Optional<User> update(User user);

    /**
     * @param newUser newUser
     * @return user
     */

    Optional<User> save(User newUser);

    /**
     * @param userID userID
     * @return boolean
     */

    boolean delete(UserID userID);
}
