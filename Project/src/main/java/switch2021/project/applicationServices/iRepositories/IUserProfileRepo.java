package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.aggregates.UserProfile.UserProfile;
import switch2021.project.entities.valueObjects.vos.UserProfileID;

import java.util.List;
import java.util.Optional;


public interface IUserProfileRepo {

    /**
     * The repository should be able to find a object using given Identity.
     * @param userProfileID userProfileID
     * @return userProfile
     */
    Optional<UserProfile> findByUserProfileID(UserProfileID userProfileID);

    /**
     * Finds all user profile
     * @return list
     */
    List<UserProfile> findAll();

    /**
     * Save user story
     * @param profile profile
     * @return userProfile
     */
    Optional<UserProfile> save(UserProfile profile);

    boolean existsByUserProfileId(UserProfileID userProfileID);

    boolean deleteById (UserProfileID userProfileID);
}
