package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.valueObjects.vos.ResourceIDReeng;
import switch2021.project.entities.aggregates.Resource.Resource;
import switch2021.project.entities.valueObjects.vos.ProjectID;
import switch2021.project.entities.valueObjects.vos.UserID;


import java.util.List;
import java.util.Optional;

public interface IResourceRepo/* extends CrudRepository<Object, Long> */{

    /**
     * The repository should be able to find a object using given Identity.
     * @param resourceId  resourceId
     * @return resource
     */

    Optional<Resource> findById (ResourceIDReeng resourceId);

    /**
     * @param resourceId resourceId
     * @return boolean
     */

    boolean existsById(ResourceIDReeng resourceId);

    /**
     * Finds all objects from this repository with any parameter
     * @return list
     */

    List<Resource> findAll();

    /**
     * @param projectId projectId
     * @return list
     */

    List<Resource> findAllByProject(ProjectID projectId);

    /**
     * @param userId userId
     * @return list
     */

    List<Resource> findAllByUser(UserID userId);


    /**
     * Save an object in the list of the repository
     * @param newResource newResource
     * @return resource
     */
    Optional<Resource> save(Resource newResource);

    /**
     * @param id id
     * @return boolean
     */
    boolean deleteByResourceID(ResourceIDReeng id);
}
