package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.aggregates.Sprint.Sprint;
import switch2021.project.entities.valueObjects.vos.ProjectID;
import switch2021.project.entities.valueObjects.vos.SprintID;

import java.util.List;
import java.util.Optional;

public interface ISprintRepo {


    /**
     * Find a List Of Sprints.
     * @return list
     */

    List<Sprint> findAllSprints();

    /**
     * Find a List Of Sprint by ID.
     * @param id id
     * @return sprint
     */

    Optional<Sprint> findBySprintID(SprintID id);

    /**
     * Find a List Of All Sprints by Project ID.
     * @param projectID projectID
     * @return sprint
     */

    List<Sprint> findAllSprintsByProjectID(ProjectID projectID);

    /**
     * Find the Current Sprint.
     * @return sprint
     */

    Sprint findCurrentSprint();

    /**
     * Save Sprint.
     * @param newSprint newSprint
     * @return sprint
     */

    Optional<Sprint> save(Sprint newSprint);

    /**
     * Delete a Sprint.
     * @param sprintID sprintID
     * @return boolean
     */

    boolean deleteSprint(SprintID sprintID);

}

