package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.aggregates.UserStory.UserStory;
import switch2021.project.entities.valueObjects.vos.UserStoryID;

import java.util.List;
import java.util.Optional;

public interface IUserStoryRepo {

    /**
     * Finds a user story using given ID
     * @param userStoryID userstoryID
     * @return userStory
     */
    Optional<UserStory> findByUserStoryId(UserStoryID userStoryID);


    /**
     * Finds all user story
     * @return list
     */
    List<UserStory> findAll();

    /**
     * @param projectId projectId
     * @return list
     */

    List<UserStory>  findProductBacklog(String projectId);

    /**
     * Save user story
     * @param newUserStory newUserStory
     * @return userStory
     */

    Optional<UserStory> save(UserStory newUserStory);

    /**
     * @param userStory userStory
     * @return userStory
     */

    Optional<UserStory> update(UserStory userStory);

    /**
     * @param usId usId
     * @return boolean
     */

    boolean deleteByUserStoryId(UserStoryID usId);
}
