package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.aggregates.Typology.Typology;
import switch2021.project.entities.valueObjects.vos.TypologyID;

import java.util.List;
import java.util.Optional;

public interface ITypologyRepo {

    /**
     * The repository should be able to find an object using given Identity.
     * @param id id
     * @return typology
     */
    Optional<Typology> findByTypologyId(TypologyID id);


    /**
     * Finds all objects from this repository.
     * @return list
     */
    List<Typology> findAll();


    /**
     * Save an object in the list of the repository.
     * @param typology typology
     * @return boolean
     */
    boolean save(Typology typology);


    /**
     * Check if the description already exists.
     * @param id id
     * @return boolean
     */
    boolean existsByTypologyId(TypologyID id);


    /**
     * Delete the object from this repository.
     * @param id id
     * @return boolean
     */
    boolean deleteByTypologyId(TypologyID id);
}
