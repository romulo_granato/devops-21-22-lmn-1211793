package switch2021.project.applicationServices.iRepositories;

import switch2021.project.entities.valueObjects.vos.EffortEstimate;

public interface IEffortEstimateFactory {

    /**
     * @param effort effort
     * @return effortEstimate
     */
    EffortEstimate create(double effort);
}
