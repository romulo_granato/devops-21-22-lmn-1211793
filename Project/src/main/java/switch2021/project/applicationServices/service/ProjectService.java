package switch2021.project.applicationServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;
import switch2021.project.applicationServices.iRepositories.IResourceRepo;
import switch2021.project.applicationServices.iRepositories.ITypologyRepo;
import switch2021.project.applicationServices.iRepositories.IUserRepo;
import switch2021.project.dtoModel.dto.*;
import switch2021.project.entities.aggregates.Typology.Typology;
import switch2021.project.entities.valueObjects.vos.*;
import switch2021.project.entities.factories.factoryInterfaces.IProjectFactory;
import switch2021.project.entities.valueObjects.voFactories.voInterfaces.IUserIDFactory;
import switch2021.project.applicationServices.iRepositories.IProjectRepo;
import switch2021.project.dtoModel.mapper.ProjectMapper;
import switch2021.project.entities.aggregates.Project.Project;
import switch2021.project.entities.aggregates.Resource.ManageResourcesService;
import switch2021.project.entities.aggregates.Resource.Resource;
import switch2021.project.entities.valueObjects.vos.enums.ProjectStatusEnum;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ProjectService {

    @Autowired
    private IProjectRepo projRepo;
    @Autowired
    private ITypologyRepo iTypologyRepo;
    @Autowired
    private IProjectFactory iProjectFactory;
    @Autowired
    private ProjectMapper projMapper;
    @Autowired
    private IUserRepo userRepo;
    @Autowired
    private IUserIDFactory userIDFactory;
    @Autowired
    private IResourceRepo resRepo;
    @Autowired
    private ManageResourcesService resService;


    public ProjectService() {
    }

    /**
     * @param projDTO projDTO
     * @return OutputProjectDTO
     * @throws Exception Project already exist.
     */

    public OutputProjectDTO createAndSaveProject(ProjectDTO projDTO) throws Exception {

        Project newProject = iProjectFactory.createProject(projDTO);

        newProject.setProjectCode(new ProjectID("Project_"+ LocalDate.now().getYear() + "_" + (projRepo.findAll().size()+1)));

        Optional<Project> savedProject = projRepo.save(newProject);

        OutputProjectDTO projectDTO;

        if (savedProject.isPresent()) {
            projectDTO = projMapper.model2Dto(savedProject.get());
        } else {
            throw new Exception("Project already exist.");
        }
        return projectDTO;
    }

    /**
     * @param id id
     * @param editProjectInfoDTO editProjectInfoDTO
     * @return OutputProjectDTO
     */

    public OutputProjectDTO updateProjectPartially(String id, EditProjectInfoDTO editProjectInfoDTO) {
//        String[] x = id.split("_");
        ProjectID projID = new ProjectID(id);

        Optional<Project> opProject = projRepo.findById(projID);
        Project proj;

        if (opProject.isPresent()) {
            proj = opProject.get();
            proj.setProjectName(new Description(editProjectInfoDTO.projectName));
            proj.setDescription(new Description(editProjectInfoDTO.description));
            proj.setStartDate(LocalDate.parse(editProjectInfoDTO.startDate));
            proj.setNumberOfSprints(new NumberOfSprints(Integer.parseInt(editProjectInfoDTO.numberOfSprints)));
            proj.setBudget(new Budget(Integer.parseInt(editProjectInfoDTO.budget)));
            proj.setSprintDuration(new SprintDuration(Integer.parseInt(editProjectInfoDTO.sprintDuration)));

            proj.setProjectStatus(ProjectStatusEnum.valueOf(editProjectInfoDTO.projectStatus));
            /*proj.setCustomer(new Customer(editProjectInfoDTO.customer, "email@email.pt", 123456789));*/
            proj.setEndDate(LocalDate.parse(editProjectInfoDTO.endDate));

            if(iTypologyRepo.existsByTypologyId(new TypologyID(new Description(editProjectInfoDTO.typology)))) {
                proj.setTypology(new Typology(new TypologyID(new Description(editProjectInfoDTO.typology))));
            }

            Optional<Project> savedProject = projRepo.save(proj);

            return savedProject.map(project -> projMapper.model2Dto(project)).orElse(null);
        }

        return null;
    }

    /**
     * @param projects projects
     * @return List
     */


    private List<OutputProjectDTO> createProjectDTOList(List<Project> projects) {

        List<OutputProjectDTO> allProjectsDto = new ArrayList<>();

        for (Project proj : projects) {

            OutputProjectDTO projDto = projMapper.model2Dto(proj);

            allProjectsDto.add(projDto);

        }

        return allProjectsDto;

    }

    /**
     * @return CollectionModel
     */

    public CollectionModel<OutputProjectDTO> showAllProjects() {

        List<Project> projects = projRepo.findAll();

        return projMapper.toCollectionDto(projects);
    }

    /**
     * @param id id
     * @return OutputProjectDTO
     * @throws Exception Project does not exist
     */

    public OutputProjectDTO showProject(String id) throws Exception {
//        String[] x = id.split("_");
        ProjectID projID = new ProjectID(id);

        Optional<Project> foundProject = projRepo.findById(projID);

        if (foundProject.isEmpty()) {
            throw new Exception("Project does not exist");
        }

        return projMapper.model2Dto(foundProject.get());
    }

    /**
     * @param id id
     * @param dateDto dateDto
     * @return List
     */

    public List<OutputProjectDTO> showCurrentProjectsByUser(String id, DateDTO dateDto) {
        List<OutputProjectDTO> projectsDto = new ArrayList<>();
        UserID userID = userIDFactory.createUserID(id);

        if (userRepo.existsById(userID)) {
            List<Resource> userResources = resRepo.findAllByUser(userID);

            List<Resource> currentUserResources = resService.currentResourcesByDate(userResources,
                                                                                    LocalDate.parse(dateDto.date));

            List<ProjectID> resourceProjects = resService.listProjectsOfResources(currentUserResources);

            List<Project> projects = new ArrayList<>();

            for (ProjectID projId : resourceProjects) {

                Project proj = projRepo.findById(projId).get();

                projects.add(proj);

            }

            for (Project proj : projects) {

                OutputProjectDTO projDto = projMapper.model2Dto(proj);

                projectsDto.add(projDto);

            }
        }
        return projectsDto;
    }

    /**
     * @param id id
     * @throws Exception Project does not exist
     */

    public void deleteProjectRequest(ProjectID id) throws Exception {
        if (!projRepo.deleteByProjectID(id)) {
            throw new Exception("Project does not exist");
        }
    }
}
