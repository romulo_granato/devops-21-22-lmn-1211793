package switch2021.project.entities.valueObjects.vos.enums;

import lombok.Getter;

@Getter
public enum ProjectStatusEnum {

    PLANNED,
    INCEPTION,
    ELABORATION,
    CONSTRUCTION,
    TRANSITION,
    WARRANTY,
    CLOSED

}
