package switch2021.project.entities.valueObjects.voFactories.voInterfaces;

import switch2021.project.entities.valueObjects.vos.ProjectRole;

public interface IProjectRoleFactory {

    ProjectRole createProjectRole(String x);

}
