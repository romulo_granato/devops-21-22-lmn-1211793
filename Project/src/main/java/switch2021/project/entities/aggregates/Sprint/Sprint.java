package switch2021.project.entities.aggregates.Sprint;

import lombok.Getter;
import lombok.Setter;
import switch2021.project.entities.valueObjects.vos.SprintID;
import switch2021.project.entities.valueObjects.vos.UserStoryOfSprint;
import switch2021.project.utils.Entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Getter
@Setter
public class Sprint implements Entity<Sprint> {

    /**
     * Attributes of Sprint
     **/
    private SprintID sprintID;
    private ScrumBoard scrumBoard;
    private LocalDate startDate;
    private LocalDate endDate;


    /**
     * Constructor of Sprint
     * @param sprintID sprintID
     */

    public Sprint(SprintID sprintID) {
        this.sprintID = sprintID;
    }


    /**
     * @param sprintID sprintID
     * @param startDate startDate
     * @param endDate endDate
     */

    public Sprint(SprintID sprintID, LocalDate startDate, LocalDate endDate) {
        this.sprintID = sprintID;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * Method to change Sprint EndDate
     * @param sprintDurationInDays sprintDurationInDays
     */

    public void changeEndDate(int sprintDurationInDays) {
        this.endDate = LocalDate.now().plusDays(sprintDurationInDays - 1L);
    }

    /**
     * @param sprID sprID
     * @return boolean
     */
    public boolean hasSprintID(String sprID) {
        return Objects.equals(this.sprintID.toString(), sprID);
    }


    /**
     * Check if this Sprint is the current Sprint
     * @return boolean
     */
    public boolean isCurrentSprint() {
        if (this.endDate == null) {
            throw new NullPointerException();
        }
        return ((this.startDate.isBefore(endDate) || this.startDate.equals(endDate))
                && (this.endDate.isAfter(startDate) || this.endDate.equals(startDate)));
    }

    /**
     * Methods to call methods from scrumBoard
     * @return List
     */

    public List<UserStoryOfSprint> getScrumBoardUserStoriesOfSprint() {
        Optional<ScrumBoard> scrumBoard = Optional.ofNullable(this.scrumBoard);

        List<UserStoryOfSprint> userStoryOfSprintList = new ArrayList<>();

        if (scrumBoard.isPresent()) {
            userStoryOfSprintList = scrumBoard.get().getUserStoriesOfSprint();
        }
        return userStoryOfSprintList;
    }


    /**
     * @param userStoryOfSprint userStoryOfSprint
     * @return boolean
     */

    public boolean saveUsInScrumBoard(UserStoryOfSprint userStoryOfSprint) {
        Optional<ScrumBoard> optional = Optional.ofNullable(this.scrumBoard);

        if (optional.isPresent()) {
            scrumBoard.getUserStoriesOfSprint().add(userStoryOfSprint);
        } else
            scrumBoard = new ScrumBoard(new ArrayList<>());
            scrumBoard.getUserStoriesOfSprint().add(userStoryOfSprint);

        return true;
    }

    @Override
    public boolean sameIdentityAs(Sprint other) {
        return other != null && sprintID.sameValueAs(other.sprintID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sprintID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sprint)) return false;
        Sprint sprint = (Sprint) o;
        return sameIdentityAs(sprint);
    }

}








