package switch2021.project.dtoModel.mapper;

import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;
import switch2021.project.entities.valueObjects.vos.UserStoryID;
import switch2021.project.interfaceAdapters.controller.UserStoryController;
import switch2021.project.dtoModel.dto.OutputUserStoryDTO;
import switch2021.project.entities.aggregates.UserStory.UserStory;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class UserStoryMapper {

    /**
     * @param newUserStory newUserStory
     * @return OutputUserStoryDTO
     */

    public OutputUserStoryDTO toDto(UserStory newUserStory) {
        OutputUserStoryDTO result = new OutputUserStoryDTO();

        result.id = idToString(newUserStory.getUserStoryID());
        result.priority = newUserStory.getPriority().getPriorityUs();
        result.description = newUserStory.getDescription().getText();
        result.timeEstimate = newUserStory.getTimeEstimate().getUsHours();

        if(newUserStory.getParentUserStory() != null) {
            result.parentUserStory = idToString(newUserStory.getParentUserStory().getUserStoryID());
        }
        if(newUserStory.getUsStartDate() != null) {result.usStartDate = newUserStory.getUsStartDate().toString();}
        if(newUserStory.getUsEndDate() != null) {result.usEndDate = newUserStory.getUsEndDate().toString();}
        if(newUserStory.getUsRefined() != null) {result.usRefined = newUserStory.getUsRefined().toString();}


        /**
         * Add HATEOAS to OUTPUT DTOs
         */

        //Add self relation
        result.add(linkTo(methodOn(UserStoryController.class).showUserStoryRequested(result.id)).withSelfRel());

        //Add collection relation
        result.add(linkTo(methodOn(UserStoryController.class).showAllUserStories()).withRel("Collection"));

        //Add delete option
        result.add(linkTo(methodOn(UserStoryController.class).deleteAUserStory(result.id)).withRel("Delete"));

        return result;


    }

    /**
     * @param userStories userStories
     * @return CollectionModel
     */

    public CollectionModel<OutputUserStoryDTO> toCollectionDto(List<UserStory> userStories) {

        CollectionModel<OutputUserStoryDTO> result = CollectionModel.of(userStories.stream()
                .map(this::toDto)
                .collect(Collectors.toList()));

        //Add HATEOAS to OUTPUT DTOs

        //Add self relation
        result.add(linkTo(methodOn(UserStoryController.class).showAllUserStories()).withSelfRel());

        return result;
    }

    /**
     * @param id id
     * @return String
     */
    private String idToString(UserStoryID id) {
        return id.getProjectID().getCode() + "&" + id.getUsTitle().getTitleUs();
    }
}
