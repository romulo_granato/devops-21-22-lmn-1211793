package switch2021.project.dtoModel.dto;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

@Getter
public class UserStoryDTO extends RepresentationModel<UserStoryDTO> {

    /**
     * Attributes
     **/
    public String projectID;
    public String title;
    public int priority;
    public String description;
    public double timeEstimate;


    /**
     * @param projectID projectID
     * @param title title
     * @param priority priority
     * @param description description
     * @param timeEstimate timeEstimate
     */

    public UserStoryDTO(String projectID, String title, int priority, String description, double timeEstimate) {
        this.projectID = projectID;
        this.title = title;
        this.priority = priority;
        this.description = description;
        this.timeEstimate = timeEstimate;
    }
}
