package switch2021.project.dtoModel.dto;

import lombok.Getter;


@Getter
public class IdDTO {
    public String id;

    /**
     * @param id id
     */
    public IdDTO(String id) {
        this.id = id;
    }
}
