package switch2021.project.dtoModel.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

@NoArgsConstructor
@Getter
public class UpdateUserStoryDTO extends RepresentationModel<UpdateUserStoryDTO> {

    /**
     * Attributes
     **/
    public int priority;
    public double timeEstimate;

    /**
     * @param priority priority
     * @param timeEstimate timeEstimate
     */

    public UpdateUserStoryDTO(int priority, double timeEstimate) {
        this.priority = priority;
        this.timeEstimate = timeEstimate;
    }
}
