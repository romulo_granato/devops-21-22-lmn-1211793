package switch2021.project.dtoModel.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Getter
@NoArgsConstructor
@Relation(collectionRelation = "User")
public class OutputUserDTO extends RepresentationModel<OutputUserDTO> {

    /**
     * Attributes
     */
    public String userName;
    public String email;
    public String function;
    public String photo;
    public String isActive;
    public String[] assignedIdProfiles;


    /**
     * @param userName userName
     * @param email email
     * @param function function
     * @param photo photo
     * @param isActive isActive
     * @param assignedIdProfiles assignedIdProfiles
     */

    public OutputUserDTO(String userName, String email, String function, String photo, String isActive,
                         String[] assignedIdProfiles) {
        this.userName = userName;
        this.email = email;
        this.function = function;
        this.photo = photo;
        this.isActive = isActive;
        this.assignedIdProfiles = assignedIdProfiles;
    }
}
