package switch2021.project.dtoModel.dto;

import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;


@Getter
public class CreateResourceDTO extends RepresentationModel<CreateResourceDTO> {
    public String systemUserID;
    public String projectId;
    public String projectRole;
    public String startDate;
    public String endDate;
    public double costPerHour;
    public double percentageOfAllocation;

    /**
     * @param systemUserID systemUserID
     * @param projectId projectId
     * @param projectRole projectRole
     * @param startDate startDate
     * @param endDate endDate
     * @param costPerHour costPerHour
     * @param percentageOfAllocation percentageOfAllocation
     */
    public CreateResourceDTO(String systemUserID, String projectId, String projectRole, String startDate,
                             String endDate, double costPerHour, double percentageOfAllocation) {
        this.systemUserID = systemUserID;
        this.projectId = projectId;
        this.projectRole = projectRole;
        this.startDate = startDate;
        this.endDate = endDate;
        this.costPerHour = costPerHour;
        this.percentageOfAllocation = percentageOfAllocation;
    }
}
