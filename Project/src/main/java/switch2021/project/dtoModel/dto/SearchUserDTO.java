package switch2021.project.dtoModel.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@NoArgsConstructor
@Getter
@Setter
public class SearchUserDTO extends RepresentationModel<SearchUserDTO> {
    public String name;
    public String function;
    public String profile;


    /**
     * @param name name
     * @param function function
     * @param profile profile
     */
    public SearchUserDTO(String name, String function, String profile) {
        this.name = name;
        this.function = function;
        this.profile = profile;
    }
}
