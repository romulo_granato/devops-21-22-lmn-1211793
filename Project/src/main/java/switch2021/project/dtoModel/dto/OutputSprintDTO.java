package switch2021.project.dtoModel.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

@Getter
@NoArgsConstructor
public class OutputSprintDTO extends RepresentationModel<OutputSprintDTO> {

    /**
     * Attributes
     */

    public String projectID;
    public String sprintID;
    public String name;

    /**
     * @param projectID projectID
     * @param sprintID sprintID
     * @param name name
     */

    public OutputSprintDTO(String projectID, String sprintID, String name) {
        this.projectID = projectID;
        this.sprintID = sprintID;
        this.name = name;
    }
}

