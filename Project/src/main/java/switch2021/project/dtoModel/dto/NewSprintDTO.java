package switch2021.project.dtoModel.dto;

import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Getter

public class NewSprintDTO extends RepresentationModel<NewSprintDTO> {

    /** Attributes*/

    public String projectID;
    public String name;

    /**
     * @param projectID projectID
     * @param name name
     */

    public NewSprintDTO(String projectID, String name) {
        this.projectID = projectID;
        this.name = name;
    }
}
