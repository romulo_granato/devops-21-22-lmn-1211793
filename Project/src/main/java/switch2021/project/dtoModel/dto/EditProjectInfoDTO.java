package switch2021.project.dtoModel.dto;

public class EditProjectInfoDTO {

    public String code;
    public String projectName;
    public String description;
    public String businessSector;
    public String typology;
    public String customer;
    public String startDate;
    public String endDate;
    public String numberOfSprints;
    public String budget;
    public String projectStatus;
    public String sprintDuration;
}
