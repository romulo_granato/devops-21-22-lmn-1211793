package switch2021.project.dtoModel.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@NoArgsConstructor
@Getter @Setter
public class UpdateDataDTO extends RepresentationModel<UpdateDataDTO> {

    /**
     * Attributes
     */
    public String userName;
    public String function;
    public String photo;
    public String oldPassword;
    public String newPassword;


    /**
     * @param userName userName
     * @param function function
     * @param photo photo
     * @param oldPassword oldPassword
     * @param newPassword newPassword
     */

    public UpdateDataDTO(String userName, String function, String photo, String oldPassword, String newPassword) {
        this.userName = userName;
        this.function = function;
        this.photo = photo;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }
}
