package switch2021.project.dtoModel.dto.old;

import lombok.Getter;

@Getter

public class TaskIdNameDTO {

    /**
     * Attributes
     **/

    private int taskId;
    private String taskName;

    /**
     * Constructor to test (without SINGLETON)
     * @param taskId taskId
     * @param taskName taskName
     */

    public TaskIdNameDTO(int taskId, String taskName) {
        this.taskId = taskId;
        this.taskName = taskName;
    }

}
