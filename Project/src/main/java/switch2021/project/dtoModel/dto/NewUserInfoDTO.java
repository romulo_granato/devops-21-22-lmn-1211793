package switch2021.project.dtoModel.dto;

import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;


@NoArgsConstructor
public class NewUserInfoDTO extends RepresentationModel<NewUserInfoDTO> {

    /**
     * Attributes
     */

    public String userName;
    public String email;
    public String function;
    public String password;
    public String passwordConfirmation;
    public String photo;

    /**
     * @param userName userName
     * @param email email
     * @param function function
     * @param password password
     * @param passwordConfirmation passwordConfirmation
     * @param photo photo
     */
    public NewUserInfoDTO(String userName, String email, String function, String password,
                          String passwordConfirmation, String photo) {
        this.userName = userName;
        this.email = email;
        this.function = function;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.photo = photo;
    }
}
