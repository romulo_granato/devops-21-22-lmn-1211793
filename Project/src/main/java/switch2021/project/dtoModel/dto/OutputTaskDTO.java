package switch2021.project.dtoModel.dto;

import lombok.Getter;

@Getter
public class OutputTaskDTO {

    public String name;
    public String description;
    public String effortEstimate;
    public String type;
    public String responsible;

}
