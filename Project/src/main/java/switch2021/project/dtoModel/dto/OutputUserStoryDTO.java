package switch2021.project.dtoModel.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;


@Getter @Setter
@NoArgsConstructor
@Relation(collectionRelation = "User Stories")
public class OutputUserStoryDTO extends RepresentationModel<OutputUserStoryDTO> {

    /**
     * Attributes
     **/
    public String id;
    public int priority;
    public String description;
    public double timeEstimate;
    public String parentUserStory;
    public String usStartDate;
    public String usEndDate;
    public String usCancelled;
    public String usRefined;

    /**
     * @param id id
     * @param priority priority
     * @param description description
     * @param timeEstimate timeEstimate
     * @param parentUserStory parentUserStory
     * @param usStartDate usStartDate
     * @param usEndDate usEndDate
     * @param usCancelled usCancelled
     * @param usRefined usRefined
     */

    public OutputUserStoryDTO(String id, int priority, String description, double timeEstimate,
                              String parentUserStory, String usStartDate, String usEndDate, String usCancelled,
                              String usRefined) {
        this.id = id;
        this.priority = priority;
        this.description = description;
        this.timeEstimate = timeEstimate;
        this.parentUserStory = parentUserStory;
        this.usStartDate = usStartDate;
        this.usEndDate = usEndDate;
        this.usCancelled = usCancelled;
        this.usRefined = usRefined;
    }
}
