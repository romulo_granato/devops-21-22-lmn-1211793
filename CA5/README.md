# CA5 Report

In this CA we must to understand what is containerization and how it works and build some of them. For this I must use Docker tool.

## Continuous Integration and Delivery

Continuous integration (CI) and continuous delivery (CD), also known as CI/CD, embodies a culture, operating principles, and a set of practices that application development teams use to deliver code changes more frequently and reliably.

CI/CD is a best practice for devops teams. It is also a best practice in agile methodology. By automating integration and delivery, CI/CD lets software development teams focus on meeting business requirements while ensuring code quality and software security.

Continuous integration(CI) is a coding philosophy and set of practices that drive development teams to frequently implement small code changes and check them in to a version control repository (Bitbucket, GitHub or others).
Continuous integration establishes an automated way to build, package, and test their applications. Having a consistent integration process encourages developers to commit code changes more frequently, 
which leads to better collaboration and code quality.

Continuous delivery(CD) picks up where continuous integration ends, and automates application delivery to selected environments, including production, development, and testing environments.
Continuous delivery is an automated way to push code changes to these environments.

## CI/CD Tools

A devops engineer uses a CI/CD tool such as Jenkins, CircleCI, AWS CodeBuild, Azure DevOps, Atlassian Bamboo, Argo CD, Buddy, Drone, or Travis CI to automate the steps and provide reporting.

For example, Jenkins users define their pipelines in a Jenkinsfile that describes different stages such as build, test, and deploy. Environment variables, options, secret keys, certifications, and other parameters are declared in the file and then referenced in stages.

CI/CD tools typically support a marketplace of plugins. For example, Jenkins lists more than 1,800 plugins that support integration with third-party platforms, user interface, administration, source code management, and build management.

Once the development team has selected a CI/CD tool, it must ensure that all environment variables are configured outside the application. CI/CD tools allow development teams to set these variables, mask variables such as passwords and account keys, and configure them at the time of deployment for the target environment.

Continuous delivery tools also provide dashboard and reporting functions, which are enhanced when devops teams implement observable CI/CD pipelines. Developers are alerted if a build or delivery fails.
The dashboard and reporting functions integrate with version control and agile tools to help developers determine what code changes and user stories made up the build.

## Jenkins

Jenkins is an open-source automation tool written in Java with plugins built for Continuous Integration purposes. Jenkins is used to build and test your software projects continuously making it easier for developers to integrate changes to the project, and making it easier for users to obtain a fresh build.
It also allows you to continuously deliver your software by integrating with a large number of testing and deployment technologies.

With Jenkins, organizations can accelerate the software development process through automation. Jenkins integrates development life-cycle processes of all kinds, including build, document, test, package, stage, deploy, static analysis, and much more.

Jenkins achieves Continuous Integration with the help of plugins. Plugins allow the integration of Various DevOps stages. If you want to integrate a particular tool, you need to install the plugins for that tool. For example Git, Maven 2 project, Amazon EC2, HTML publisher etc.

The image below depicts that Jenkins is integrating various DevOps stages:

![Jenkins-Plugins](Jenkins-integration-plugins.jpg)

### How Jenkins Work

Jenkins runs as a server on a variety of platforms including Windows, MacOS, Unix variants and Linux. It requires a Java 8 VM and above and can be run on the Oracle JRE or OpenJDK. Usually, Jenkins runs as a Java servlet within a Jetty application server.

To operate Jenkins, pipelines are created. A pipeline is a series of steps the Jenkins server will take to perform the required tasks of the CI/CD process. These are stored in a plain text Jenkinsfile. The Jenkinsfile uses a curly bracket syntax that looks similar to JSON.
Steps in the pipeline are declared as commands with parameters and encapsulated in curly brackets. The Jenkins server then reads the Jenkinsfile and executes its commands, pushing the code down the pipeline from committed source code to production runtime.
A Jenkinsfile can be created through a GUI or by writing code directly.

## Part 1

To this exercise need to create a pipeline to gradle_basic_demo project and execute that with Jenkins Server.

### Steps

It must install Jenkins. Download Jenkins WAR file (in any the directory), go to terminal and run with the command <java -jar jenkins.war> inside the directory with jenkins WAR file.

After this steps I can browse to localhost:8080 until Jenkins page appears. Then copy the automatically-generated alphanumeric password (from terminal) and paste this password into the Administrator password field and click Continue.

Now it has Jenkins Server GUI to create new pipelines and manage the project.

**Step 1** - Create a Jenkinsfile inside the root folder to the project. <TOUCH Jenkinsfile>

**Step 2** -  Edit Jenkinsfile with necessary instructions. It can use any text editor.

Here will need to use the main block inside jenkinsfile, as:

*pipeline* - all valid Declarative Pipelines must be enclosed within a pipeline block.

*agent* - the agent section specifies where the entire Pipeline will execute in the Jenkins environment depending on where the agent section is placed. The section must be defined at the top-level inside the pipeline block.

*stages* - containing a sequence of one or more stage directives, the stages section is where the bulk of the "work" described by a Pipeline will be located.

*stage* - the stage directive goes in the stages section and should contain a steps section. Practically speaking, all of the real work done by a Pipeline will be wrapped in one or more stage directives.

*steps* - the steps section defines a series of one or more steps to be executed in a given stage directive.

*post* -  the post section defines one or more additional steps that are run upon the completion of a Pipeline’s.

Create four stages, each one with its specific responsibility as checkout, assemble, test and archiving.

*checkout* -  clone the repository with the project that will be managed by Jenkins. Use <git> command

*assemble* -  build the project without run tests. Use <dir()> to define where has the project and then inside that use <bat> to run shell command (./gradlew assemble).

*test* -  run the tests of the project. Use <dir()> then inside that use <bat> to run shell command )(./gradlew test).

*archiving* -  arquive the files created when build the project. Use <dir()> and then inside that use <archiveArtifacts [path]> to arquive the files inside the "path" specified.

On "post" section use "always" as post-condition block and inside that block use <junit [path]> to publish the test results inside Jenkins Server GUI. The "path" is where the test results are.

**Step 3** - create a new pipeline on Jenkins Server GUI using SCM (where has the project) and edit the path where has the jenkinsfile from the project. Because the SCM is public do not need to use credentials.

**Step 4** - build the pipeline created and if everything are okay the build will be a success. When the build are running it can see all steps declared on jenkinsfile.

**Step 5** - now it can found the artifacts arquiving on jenkins.

To define parameter inside another parameter it need to use curly brackets to enclosure these actions. This sintax are called blocks.

## Part 2

To this exercise need to create another pipeline to gradle_basic_demo project and execute that with Jenkins Server.

#### Steps

Same steps from Part 1 adding two stages to create and publish Javadoc from project and create and publish Docker image from this project.

**Step 1** - Create a Jenkinsfile inside the root folder to the project. <TOUCH Jenkinsfile>

**Step 2** -  Edit Jenkinsfile with necessary instructions. It can use any text editor.

Here will need to use the main block inside jenkinsfile, as:

*pipeline* - all valid Declarative Pipelines must be enclosed within a pipeline block.

*agent* - the agent section specifies where the entire Pipeline will execute in the Jenkins environment depending on where the agent section is placed. The section must be defined at the top-level inside the pipeline block.

*stages* - containing a sequence of one or more stage directives, the stages section is where the bulk of the "work" described by a Pipeline will be located.

*stage* - the stage directive goes in the stages section and should contain a steps section. Practically speaking, all of the real work done by a Pipeline will be wrapped in one or more stage directives.

*steps* - the steps section defines a series of one or more steps to be executed in a given stage directive.

*post* -  the post section defines one or more additional steps that are run upon the completion of a Pipeline’s.

Create six stages, each one with its specific responsibility as checkout, assemble, test, javadoc, archiving and docker image.

*checkout* -  clone the repository with the project that will be managed by Jenkins. Use <git> command

*assemble* -  build the project without run tests. Use <dir()> to define where has the project and then inside that use <bat> to run shell command (./gradlew assemble).

*test* -  run the tests of the project. Use <dir()> then inside that use <bat> to run shell command )(./gradlew test).

*javadoc* -  create a javadoc file from project. Use <dir()> and inside that use <bat> to run shell command (./gradlew javadoc). Then use <publishHTML> to create html file from javadoc created and define where will be persisted.

To use "publishHTML" need to install that plugin on Jenkins Server. To do this go to Jenkins Server GUI - manage jenkins - manage plugins - availables - search to publishHTML and install that. Now the Jenkins Server will be able to recoginize the publishHTML parameter.

*archiving* -  arquive the files created when build the project. Use <dir()> and then inside that use <archiveArtifacts [path]> to arquive the files inside the "path" specified.

*docker image* -  build and publish docker image from project inside Docker Hub. Use <dir()>, inside that use <script> and then inside that use <docker.withRegistry()>, <docker.builkd()> and <push()>.

On "post" section use "always" as post-condition block and inside that block use <junit [path]> to publish the test results inside Jenkins Server GUI. The "path" is where the test results are.

**Step 3** - create a new pipeline on Jenkins Server GUI using SCM (where has the project) and edit the path where has the jenkinsfile from the project. Because the SCM is public do not need to use credentials.

**Step 4** - build the pipeline created and if everything are okay the build will be a success. When the build are running it can see all steps declared on jenkinsfile.

**Step 5** - now it can found the artifacts arquiving on jenkins.

## Alternatives CI/CD Tools

As describe above, we have a lot of CI/CD tools. Jenkins is undoubtedly the most well-known tool for building CI/CD pipelines. A robust, open-source project, Jenkins has a devoted community worldwide.
Another great feature of Jenkins is the built-in, community-driven plugin system. With thousands of plugins at your fingertips, it’s easy to extend and add new functionalities.
However, this ability to fulfill virtually any type of use case with thousands of possible combinations also makes it overwhelming for less experienced engineers to set up an optimal CI/CD pipeline.

CircleCI is a SaaS solution that became popular among frontend developers due to its polished JavaScript support and friendly developer experience.

Another popular solutions are Travis CI, GitLab, Bamboo and many others.

To this exercise I chose CircleCI to implement.

### CircleCI

CircleCI is primarily a cloud-based CI orchestration tool (not need to be installed). There is also an Enterprise version which can be setup on one’s own infrastructure. It was founded in 2011 and is based out of San Francisco.
This tool helps in automating installation and delivery processes. It is quite simple to configure and maintain.

CircleCI reduces the overhead of having a dedicated server as it is cloud-based. The enterprise version is also low on maintenance. The cloud-based platform offers credit-based plans that are scalable and help in deploying applications faster.

CircleCI uses *config.yaml* file inside *.circleci* folder at the root of the repository/project.

#### Using CircleCI

The main sctructure of that file is:

*orbs* - a reusable package of YAML configuration that condenses repeated pieces of config into a single line of code. Equivalent to plugin in Jenkins.

*version* - indicates the version of the CircleCI platform you are using. 2.1 is the most recent version.

*executors* - define the environment in which the steps of a job will be run, allowing you to reuse a single executor definition across multiple jobs.

*jobs* - the jobs level contains a collection of children, representing your jobs. You specify the names for these jobs, for example, build, test, deploy. Equivalent to <stages> in Jenkins

> Each *job* consists of the job’s name as a key and a map as a value. A name should be case insensitive unique within a current jobs list. The value map has the following attributes: docker, steps, working_directory and others (https://circleci.com/docs/2.0/configuration-reference/).
Equivalent to <stage> in Jenkins.
>
>> *working_directory* - in which directory to run the steps. Equivalent to <dir()> in Jenkins.
>>
>> *steps* - A list of steps to be performed. Same as Jenkins.
>>
>>> *run* - used for invoking all command-line programs. It is a step-type parameter. Equivalent to <bat> in Jenkins.
>>>
>>>> *command* - used to aggregate more than one run command. Each line specified the command that must be run.
>>>
>>> *store_artifacts* - will store the [path] chose on Artifacts of the build. Equivalent to <archiveArtifacts> in Jenkins.

>>> *store_test_results* - will store the test results (at the given [path]) on Tests of the build. It is like Post{junit [path]} in Jenkins.

*workflows* - define a list of jobs and their run order.

>*path* is a parameter from step-type that specified the folder used by that. 

#### Steps

**Step 1** - To use CircleCI need to login on your web application. It can use the github or bitbucket credentials (CircleCI are integrated with these SCMs).

**Step 2** - If chose the SCM credentials, the app aldeady define a project from that repository. If not, must to create a project.

**Step 3** - create .circleci folder inside root repository folder and then create a new config.yaml file inside this folder.

**Step 4** - edit the config.yaml file with necessary steps.

It used the above parameters to edit my config.yaml file. Create two jobs, build and publishLatestToHub. Build job has five steps with checkout, assemble, test, javadoc and arquive steps.
The other job has the responsability to create a docker image and publish that to Docker Hub.

It used executors to define the enviroment shared by the two steps, like working_directory, create a variable IMAGE_NAME and define auth to login on Docker Hub.

It used workflows to define the run order and specified the context (to use global enviroment variables) and dependencies between jobs.

It created two global enviroment variables to define the username and password to login on Docker Hub. To do that must create a context on Organization Settings from CircleCI Web App and inside this context create the variables.

**Step 5** - set up project with circleci.yaml file from repository.

**Step 6** - Now all new commit to that repository will invoke a build pipeline from the config.yaml file. And can run the pipeline inside CircleCI Web App chosing the pipeline created and rerun command.

If everything are okay, the project will use CircleCI as CI/CD tool like Jenkins.

