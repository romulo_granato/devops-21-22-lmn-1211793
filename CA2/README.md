# CA2 Report

In this CA we must to understand how build tools work by implementing GRADLE as choice tool.

## GRADLE Build Tool

Gradle is an open-source build automation tool focused on flexibility and performance. Gradle build scripts are written using a Groovy or Kotlin DSL. General features:

Highly customizable — Gradle is modeled in a way that is customizable and extensible in the most fundamental ways.

Fast — Gradle completes tasks quickly by reusing outputs from previous executions, processing only inputs that changed, and executing tasks in parallel.

Powerful — Gradle is the official build tool for Android, and comes with support for many popular languages and technologies.

The core of Gradle is a language for dependency based programming. In Gradle terms this means that you can define tasks and dependencies between tasks. 
Gradle guarantees that these tasks are executed in the order of their dependencies, and that each task is executed only once.

### Build phases
A Gradle build has three distinct phases.

#### Initialization
Gradle supports single and multi-project builds. During the initialization phase, Gradle determines which projects are going to take part in the build, and creates a Project instance for each of these projects.

#### Configuration
During this phase the project objects are configured. The build scripts of all projects which are part of the build are executed.

#### Execution
Gradle determines the subset of the tasks, created and configured during the configuration phase, to be executed. Gradle then executes each of the selected tasks.

### Main features

Gradle has two main documents to configure how it work.

#### Settings.Gradle

The place where this document is will determine if you have a multi-project build or a single project build. This happens because this file will define how build work. Basically, in this document you will configure the build of your project.
Usually you will define the name of your project here too. 

#### Build.Gradle

This is the most important file of the gradle workflow. Here is defined plugins, dependencies, tasks  and more that your project needed to compile, build and run. It is similar to POM file if use MAVEN build tool.

Here is all work that we must to do about our class assigment. Install plugins, define dependencies, create tasks and etc.

## CA steps

### Part 1
Add gradle_basic_demo project to CA2/Part1 folder from remote repository devops-21-22-lmn-1211793. This project already uses gradle build tool.

Study this project and run some tasks and commands to see how it works, like runClient task and "$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001" command.

The step 3 ask to create a new task to run the server. I create this task on build.gradle file as runServer and did this similar to runClient task that already exist in the project, 
changing mainClass property to "basic_demo_ChatServerApp" and args to only "59001" port. Now i can call this task from terminal using "$ ./gradlew runServer" to run the server of the project, instead of using the java command above.

The step 4 ask to add a unit test to the project and make it work. Basically I create a new java class as AppTest, then added the code present on the CA. 
But to the test work I must add a new dependency to build.gradle file on "dependencies" scripts to implement jUnit framework, like "implementation 'junit:junit:4.13.1'". Now my project will compile and build.

The step 5 ask to add a new task of type Copy. This task must make a backup of the src folder of the app.
To do that I create a new task called backupSRC using "from" and "into" commands that are able to specify which folder the task must copy and to where. 
This methods are able because I classify this task as Copy type. This allows many useful methods and properties for the task.

The step 6 ask to add a new task of type Zip. This task must create a zip file from src folder.
As in the previous step, I define this task as Zip type and this allows many useful methods. To this situation, I use "from" (which will use) and "destinationDir()" (where will save) methods and "arquiveFileName" property (the name of the new file created).

To finished this part I created a tag (using git tag command) called ca2-part1.

### Part 2

Create a new branch tut-basic-gradle to work with the new project.

Create a new Gradle project to CA2/Part2 folder from remote repository devops-21-22-lmn-1211793, using https://start.spring.io and configure it.

The atep 4 ask to delete src folder from the created project, because we want to use the code from the basic tutorial (React and Spring Data Rest) project.

The step 5 ask to add the basic tutorial src folder to the project and also webpack.config.js and package.json files. Besides we must delete built folder from "src/main/resources/static/".

Now, if I use bootRun task to run the APP will be okay, but the web page will be empty, instead of show the frontend code (employee table). This happens because the frontend plugin is not installed on the build.gradle file.

The step 8 ask to install de frontend plugin (org.siouan.frontend). To do this I must to add "id 'org.siouan.frontend-jdk11' version '6.0.0'" this plugin to plugins script from build.gradle file. And then configure that with some informations how ask in the step 9 and 10.

Now I can execute build and bootRun task to see the application working.

The step 13 ask to create a task to copy the generated jar file (from 'build/libs') to 'dist' folder. To do that I create a new task called copyJAR with Copy type to allows useful methods like 'from', 'include' and 'into'. 
Using this methods I can set which folder/file the task will copy and the destination of this copy. The 'include' method will filter the specified file/folder name/type to copy.

The step 14 ask to create another task to delete all the files generated by webpack, located at 'src/resources/main/static/built/'. This task must be executed automatically by gradle before the task clean (aldeady exist).
The new task was called deleteAll and has the type of Delete. Now I can use the delete method to select which folder the task will remove. 
Besides, to do that task run always before the clean task, I set the clean task with 'dependsOn' property. This configuration ask to Gradle that the clean task only can run after that task. Now, if I call clean task, the deleteAll task will run first.

If all steps are ready and the project are working, I must merge the created branch to the master branch. And then I can push them to my remote repository.

To finished this part I created a tag (using git tag command) called ca2-part2.

## Alternative build tools.

We have many build tools available. The best known are Ant and Maven, but has Bazel and SBT too, besides others. Ant and Maven uses XML language while SBT uses Scala and Bazel uses Starlark. 
Ant is the oldest, it was released in 2000. In 2004 was released Maven, then in 2008 was SBT (Simple build tool) and Bazel in 2015.

Bazel tool is an open-sourced part of the Google build tool (called Blaze).

## Bazel Build Tool

Bazel uses WORKSPACE and BUILD.bazel files to configure the build tool of the project.

The workspace and build files will be on the root directory of your project. There will be others build files thrown the project. Each build file represent a TARGET. Each target will be responsible to manage that piece of code. 
This approach enable to optimize all build process.

### Create Bazel Project

Create a new folder called Bazel. Inside that we copied the react-and-spring-data-rest-basic project. Then delete all gradle files and create WORKSPACE and BUILD.bazel files.

#### Configure BUILD.bazel file

Create 'java_binary' script with name, srcs, main_class and deps properties. The name will be the call to the app, like 'bazel build <name>'. The srcs is where the app must run. The main_class is the Java class that has the main method.

The deps property specify dependency implementation that this project needed, like jUnit, spring-boot, data REST, H2, JPA and thymeleaf.

#### Configure WORKSPACE file

To work with JAVA RULES, we copy some scripts from web to configure the project. This able the project to use maven repo to import some dependencies.