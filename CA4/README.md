# CA4 Report

In this CA we must to understand what is containerization and how it works and build some of them. For this I must use Docker tool.

## Containerization

Containerization is the packaging together of software code with all it’s necessary components like libraries, frameworks, and other dependencies so that they are isolated in their own "container."

This is so that the software or application within the container can be moved and run consistently in any environment and on any infrastructure, independent of that environment or infrastructure’s operating system.
The container acts as a kind of bubble or a computing environment surrounding the application and keeping it independent of its surroundings. It’s basically a fully functional and portable computing environment.

## Containerization vs Virtualization

Containerization and virtualization are similar in that they both allow for full isolation of applications so that they can be operational in multiple environments. Where the main differences lie are in size and portability. 

VMs are the larger of the two, typically measured by the gigabyte and containing their own OS. The increased resources available to VMs allows them to abstract, split, duplicate, and emulate entire servers, operating systems, desktops, databases, and networks.

Containers are much smaller, typically measured by the megabyte and not packaging anything bigger than an app and its running environment. 

Where VMs work well with traditional, monolithic IT architecture, containers were made to be compatible with newer and emerging technology like clouds, CI/CD, and DevOps.

## Docker Tool

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. The software that hosts the containers is called Docker Engine. It was first started in 2013.

Docker uses a Dockerfile to build images by reading its instructions. Dockerfile is a text file that contains all commands, in order, needed to build a given image.

The main commands used in Dockerfile are: *FROM, RUN, COPY, WORKDIR and CMD*. The FROM command is the only one mandatory.

## Part 1

To this exercise need to package and execute the chat server from gradle_basic_demo repository in a container using docker.

### Steps

It must install docker. As my PC has a windows 10 home as operating system and this OS not have by default some configuration I need to follow some steps explained in this site: https://www.padok.fr/en/blog/docker-windows-10.

After this steps I can use docker desktop natively on my OS. Then I can start Docker Desktop to follow the below steps. To do this I use my Hyper terminal.	

**Step 1** - Create a Dockerfile inside the folder to this project. TOUCH Dockerfile

**Step 2** -  Edit dockerfile with necessary instructions to run the chat server from app. It can use any text editor.

Here will need to use the main commands inside dockerfile, as:

*FROM* - to chose a known image to initiate this new image that will be configured with new commands to this project. This command will search the known image in the docker installed or on Registry (Docker Hub or Docker Store).

*RUN* - to add new configuration to the known image choosen above. It can use as many RUN command as necessary. Here can add new applications, folders, files and much more to this new image.

*COPY* - to copy something from host to container image.

*WORKDIR* -  to change and create (if does not exist) to other working directory inside the image.

*CMD* - it can be used only once. Here can put any command that will be execute when the container is runnning.

*EXPOSE* -  to specify in which port this image can be access from outside.

**Step 3** - create a image from Dockerfile. Inside the directory that has the Dockerfile use *<DOCKER BUILD .>* (will be create a new image from Dockerfile inside the working directory without a tag/name) or *<DOCKER BUILD -t [image name]>*.

It can be create a new image outside the directory that has the Dockerfile using -f parameter. *<DOCKER BUILD -f [path to Dockerfile]>* .

To check which images has on docker. *<DOCKER IMAGE LS>* or *<DOCKER IMAGES>*.

To create a new image with new tag and version from a image that already exist. *<DOCKER TAG [IMAGE ID or NAME/TAG] [NEW NAME/TAG:VERSION]>*. This will be useful to configure the image to be pushed to registry (Docker Hub or Docker Store).

To remove a image. DOCKER RMI *<[image ID/name/tag]>*

**Step 4** - create a container from the image. *<DOCKER RUN [IMAGE ID or NAME/TAG]>*

It can be used a few parameters to configure this initialization as: 

*<-d>* if want to run the image in the background. This will enable prompt after the container creation.

*<-p>* if want to configure the comunication from container and host. Here can choice which host port will comunicate with container port.

*<-i>* and <-t> setups an interactive session.

*<--name>* to define a name to the container.

*[command]* after the image name/id/tag to specify a command to be run on the container once it is started.

To manage the containers the docker has some commands as:

*<DOCKER PS>* - To list all containers running

*<DOCKER PS -a>* - To list all containers running or not.

*<DOCKER STOP [container ID/name]>* - To stop the container.

*<DOCKER RM [container ID/name]>* - To remove the container.

*<DOCEKR exec -it [container ID/name] [command]>* - To run a command inside the container.

**Step 5** - Now the container is running and the application are ready to be used. It can be checked when I run the chat client inside the host using ./gradlew runClient inside the app directory.

**Step 6** - Repeat steps 3, 4 and 5 to the second version of the same application. But now the image has only the jar file and need to run the chat server.

I must push the images created to DOCKER HUB. To do that I need to change the image tag and version using the command mentioned above. *<DOCKER TAG [image ID/name/tag] [new name/tag:version]>*

Then I can use *<DOCKER PUSH [image name]:[image version]>* to send the image to registry.

PS: If container was changed after running and this change need to be saved, it can be used *<DOCKER COMMIT [container ID/name] [new image]>*.

## Part 2

To this exercise need to package and execute the tutorial react app in two containers (db and web) using docker-compose. The Docker Desktop already has compose installed.

###Compose

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.

A docker-compose file looks like this:

![docker_compose](docker_compose.png)

####Main features

*version* - is optional. This will set the version of compose that will be used.

*services* - here will add the containers to this configuration. Each container will be define by a name and will have some configurations.

*[container name]* - container name.

*image* - define a image (exist on registry) to run a container.

*build* - define the Dockerfile that will be used to build a image and run a container. If use *<.>* will be used a Dockerfile from the same directory. It can specify another directory using a path.

*ports* - specify the comunication ports from containers and host PC.

*volumes* - specify a host place (folder) that will shared data with containers. Basically in this place all generated data from container will be disponible on host side and vice versa.

*networks* - inside services can define the IP of this container. Otherwise can define a compose network. 

And has many others configurations.

#### Steps

**Step 1** - create a docker-compose.yml file. Inside the project use *<TOUCH docker-compose.yml>*.

**Step 2** - edit the docker-compose created using the features above and using https://bitbucket.org/atb/docker-compose-spring-tut-demo/ as reference.

**Step 3** - create two folders with name db and web. Then create two new Dockerfile inside each folder.

**Step 4** - edit these dockerfiles with the necessary commands to run the app. The web dockerfile was modified to install tomcat manually and to use my remote repository and my app.

**Step 5** - to execute the docker-compose file inside the project folder that has the file use *<DOCKER-COMPOSE UP>*

This command will build the images from dockerfiles, pulls images from registry, creates and starts containers, configure a bridge network for the containers and handles logs.

If all are okay, the app will be running and can be checked using host browser with the ports configured inside docker-compose file.

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT (to web)

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (to db)

It must push the images created to DOCKER HUB. *<DOCKER PUSH [image name]>*.

**Step 6** - using the volumes configured copy the database created when db was executed. *<DOCKER EXEC -it [container id/name] [/bin/bash]>* to run a shell inside the db container.

Then move the database file to folder configured as volumes. *<MV [database file] [volumes folder]>*.

Now it can see and backup the database file from host inside the folder configured as volumes.

## Kubernetes

Kubernetes is a portable, extensible, open source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation.
It has a large, rapidly growing ecosystem. Kubernetes services, support, and tools are widely available.

The name Kubernetes originates from Greek, meaning helmsman or pilot. K8s as an abbreviation results from counting the eight letters between the "K" and the "s". Google open-sourced the Kubernetes project in 2014.
Kubernetes combines over 15 years of Google's experience running production workloads at scale with best-of-breed ideas and practices from the community.

Kubernetes is a container orchestration technology. Under Kubernetes, a web application’s logic is segmented into containers. Containers are organized into an abstraction called a pod.
A pod can have one or many containers. A pod’s logic is exposed to the network by way of another Kubernetes abstraction called a service. Under Kubernetes, containers, pods, and services are hosted within a collection of one or many computers, real or virtual.
In Kubernetes parlance, a computer is called a node. Kubernetes runs over a number of nodes. The collection of nodes is called a Kubernetes cluster.

### Docker Compose vs Kubernetes

Kubernetes and Docker Compose(using as Docker Swarm) are both container orchestration frameworks. Kubernetes runs containers over a number of computers, virtual or real. Docker Compose runs containers on a single host machine.

Also, Kubernetes has scaling and fault-tolerance features that make it so Kubernetes can automatically create more pods at runtime to meet increased demand with no loss of service. 

Also, if a pod fails, Kubernetes will automatically replenish it. Docker Compose supports automatic restarts on failure but is not intended to support auto-scaling.

In the end, Docker is far simpler than Kubernetes, but Kubernetes is far more scalable than Docker.

### Using Kubernetes

The declaration of the kubernetes objects must be done with YAML files. It can create many kubernetes objects inside the same file using *< --- >* between them.

The Kubernetes working with end points, like APIs.

The Kubernetes uses *<KUBECTL>* as command-line tool.

The kubernetes objects iterate using labels and matchlabels.

**Step 1** - create a kubernetes cluster (control plane, nodes).

**Step 2** - create a YAML file for each container with some kubernetes objects: a deployment to manage version control of the pods (the deployment objetc will create a pod to execute a container and a replicaSet to maintain a stable set of replica Pods running at any given time) and; a service to expose the pod to comunicate with others.

This YAML file has some resources that must be written as:

*apiVersion* - API group that will use. Can see this with *<KUBECTL api-resources>*

*kind* - type of kubernetes object (deployment, service and more).

*metadata* - object metadatas, like name and etc.

*spec* - specification of the kubernetes object.

And others.

![kubernetes.yaml](kubernetes_example.png)

PS: when declare the service object must be choosen the type as LoadBalancer to enable to iterate with the app outside of the kubernetes cluster.

**Step 3** - create each declaration(each container, for example) with its YAML file. *<KUBECTL CREATE -f [YAML file]>*

To see the objects can use *<KUBECTL GET [pods or deploy or services or all]>*

Now the containers are running inside kubernetes cluster.

If want to fix/change some object of the cluster and update can use *<KUBECTL APPLY -f [YAML file]>*

If everything will be okay the app will be running with kubernetes as the same configuration of docker.